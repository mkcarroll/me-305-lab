'''
@file Data_Hub.py
@brief a file that serves as the hub for all data transmission in the MCU
@details This file receives transmissions via UART from the user interface and 
interprets them. If 'G' is received, the script signals the data collection task 
to begin collecting data. if 'S' is received, the script signals the data collection
task to stop collecting data. this data is then transmitted in batch to the UI.

The following is a finite state diagram for this program:

@image html DataHubFSM.png "Data Hub FSM" width=600px

@author Mitchell Carroll
'''

from pyb import UART
import utime
import Lab4_Shares

## the initial state of the system
S0_init          = 0

## state 1: wait for a command to e received
S1_wait_for_cmd  = 1

## state 2: wait for response
S2_wait_for_resp = 2

class Data_Hub_task:
    '''
    Data Hub task
    
    this task interprets commands from the UI and sends data to the UI
    '''
    def __init__(self,interval,sample_time,task_num,dbg=True):
        '''
        creates a Data_Hub task object
        @param interval defines how often the task will run
        @param sample_time defines how long samples will be taken for
        @param task_num defines what number this task is
        @param dbg a boolean that is true if we want to print debug statements
        '''
        ## defines the initial state of the system
        self.state = S0_init
        
        ## defines how much time in seonds between task runs
        self.interval = int(interval*1000)
        
        ## defines how long in seconds data will be collected for
        self.sample_time = sample_time*1000
        
        ## defines what number the task is
        self.task_num = task_num
        
        ## a boolean that is true if we want to print debug statements
        self.dbg = dbg
        
        ## creates serial communication variable
        self.ser = UART(2)
        
        ## sets the start time
        self.start_time = utime.ticks_ms()
        
        ## defines the next time that the task will run
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## a boolean that returns True if the data collection has started
        self.start_ind = False
        
        ## sets the initial run count
        self.runs = 0
        
        ## defines the serial communication variable
        self.ser = UART(2)
        
    def run(self):
        '''
        runs one iteration of the task
        '''
        ## defines the time that the task starts running
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time,self.next_time) >= 0:
            if self.state == S0_init:
                self.printTrace()
                self.transitionTo(S1_wait_for_cmd)
                
                
            if self.state == S1_wait_for_cmd:
                self.printTrace()
                if self.ser.any() != 0:
                    
                    ## a variable that contains the data read from the buffer
                    self.val = self.ser.readchar()
                    if self.val == 71:
                     
                       Lab4_Shares.datacol = True
                       self.start_ind = True
                       #self.ser.write('Data collection has begun')
                   
                    if self.start_ind == True and (self.val == 83):
                       
                        Lab4_Shares.datacol = False
                        self.start_ind = False        
                        #self.ser.write('Data collection has stopped')
                        self.transitionTo(S2_wait_for_resp)
                    
                                   
            if self.state == S2_wait_for_resp:
                self.printTrace()
                if Lab4_Shares.data:
                    self.ser.write(str(Lab4_Shares.data))
                    Lab4_Shares.data = None
                    self.transitionTo(S1_wait_for_cmd)
                    
            self.runs += 1
            
            self.next_time = utime.ticks_add(utime.ticks_ms(),self.interval)
                    
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg == True:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.task_num, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)