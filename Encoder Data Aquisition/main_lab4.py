'''
@file main_lab4.py
@brief Coordinates task timing on MCU.
@details This script runs one task that sends commands to a nucleo and waits 
for a response. This task is used specifically to collect data from a hall effect
encoder. A 'G' will start data collection, and an 'S' will stop data collection. 
Data is then transmitted to the computer in a batch from the nucleo.
@author Mitchell Carroll
'''
import pyb
import Lab4_Shares
from Data_Hub import Data_Hub_task
from encoder_getter import encoder_getter_task,encoder_driver

## timer object, input any valid timer pin combination
timer = 3

## pin 1 object input, any valid pin timer configuration
pin1 = pyb.Pin.cpu.A6

## pin 2 object input, any valid pin timer configuration
pin2 = pyb.Pin.cpu.A7

## interval object, specifies how oftern tasks will run
interval = 0.1

## specifies how long in seconds that the nucleo will sample for
sample_time = 10

## specfies how often in seconds that the encoder will be smapled. must be greater than interval.
sample_period = 0.2


## a boolean that signals a print satement for each task
dbg = False

## encoder Object
encoder = encoder_driver(timer,pin1,pin2)

## Task object for the encoder
task1 = Data_Hub_task(interval,sample_time,1,dbg) # Will also run constructor

## task object for the user interface
task2 = encoder_getter_task(encoder,interval,sample_period,sample_time,2, dbg)

# To run the task call task1.run() over and over
while True: 
    task1.run()
    task2.run()