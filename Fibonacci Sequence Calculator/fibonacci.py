'''
@file: fibonacci.py

@brief This script returns the fibonacci number at a specified index.

@details This function initially checks if the value entered is a positive 
integer. If the value is negative, the script returns "please enter a 
positive integer." If the value is not an integer, the script returns
"please enter an integer." If the initial conditions are met, the function
logs the values of th initial 2 terms of the fibonacci sequence, and then
loops through the rest of the terms leading up to the specified index 
using memoization to increase processing speed. 

'''

## @brief Defines a function "fib()" into which a positive integer can be 
## entered.

def fib(idx):

        if idx < 0:
            print('pelase enter a positive value')
        if isinstance(idx,int) == False:
            print('please enter an integer')
        else:
            if idx == 0:
                print(0)
            if idx == 1:
                print(1)
            if idx > 1:
                fibval = [0]*(idx+1)
                fibval[1] =  1
                for x in range(2,idx+1):
                    fibval[x] = fibval[x-1] + fibval[x-2]
                print(fibval[idx])
