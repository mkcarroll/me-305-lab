'''
@file controller_shares_lab7.py
@brief a file that contans shared variables for th MCU motor controller
@author Mitchell Carroll
'''
import array

## a boolean that is true to signal the motor to start
start = False

## a variable containing collected speed data from the encoder
speed = [0]

## a variable containing collected position data from the encoder
position = [0]