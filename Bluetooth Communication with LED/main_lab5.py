'''
@file main_lab5.py
@brief runs a task over and over
@details This file runs the BLE task over and over to receive inputs via 
bluetooth and flash an LED at a user specified frequency
@author Mitchell Carroll
'''

from BLE import BLE_driver
from BLE_FSM import BLE_task
import pyb

## creates a BLE driver object
BT = BLE_driver()

## defines how often in microseconds that the task will run
interval = 10000

## creates a task1 object for the BLE task
task1 = BLE_task(BT,interval)

while True:
    task1.run()