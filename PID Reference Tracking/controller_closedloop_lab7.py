'''
@file controller_closedloop_lab7.py
@brief A closed loop motor controller
@details this program receives a start and Kp value command from controller_receiver.py.
the controller then used the user defined Kp value to run a proportional motor controller.
the speed values are recorded. The controller stops when signaled by controller_receiver.py.

The following is an FSM diagram from the program:
    
@image html closedloopFSM.png "Closed Loop FSM diagram" width=600px

@author Mitchell Carroll
'''

import utime
import controller_shares_lab7
import array

## the intial state of the system
S0_init = 0

## in this state the controller waits for input
S1_wait = 1

## in this state the controller is running the motor
S2_run = 2

class controller_task:
    '''
    a class that contains the contoller task
    '''
    def __init__(self,interval,moe,enc,closedloop):
        '''
        creates a controller_task object
        @param interval specifies the time between runs in microseconds
        @param moe a motor driver object
        @param enc an encoder driver object
        @param closedloop passes in the closed loop controller class
        '''
        
        ## defines the time between runs in microseconds
        self.interval = interval
        
        ## a motor driver object
        self.moe = moe
        
        ## an encoder object
        self.enc = enc
        
        ## a closed loop controller object
        self.closedloop = closedloop
        
        ## specifies what level the motor will start at
        self.set = 0
        
        ## defines the initial state
        self.state = S0_init
        
        ## defines the number of runs that the controller has executed
        self.runs = 0
        
        ## records the starting time of the task
        self.start_time = utime.ticks_us()
        
        ## defines the next time when the task will run
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## specifies the period that samples will be taken
        self.sample_rate = 10000
        

    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## records the time that each iteration runs
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time,self.next_time) >=0:        

            if self.state == S0_init:
                
                self.transitionTo(S1_wait)
                
                
            if self.state == S1_wait:
                
                self.enc.update()
                
                self.enc.set_position(0)
                
                self.closedloop.unwind()
                
                if controller_shares_lab7.start:
                    
                    self.moe.enable()
                    
                    self.transitionTo(S2_run)  
                    
                    ## records the time that the samples begin
                    self.start_sample = utime.ticks_us()
                    
                    ## specifies that time that the next sample will be taken
                    self.next_sample = utime.ticks_add(self.start_sample,self.sample_rate)
            
            if self.state == S2_run:
                
                self.enc.update()
                
                ## Records the current speed of motor
                self.speed = self.enc.get_delta()*60/(4000*self.interval/(1000000))
                
                if utime.ticks_diff(self.curr_time,self.next_sample)>=0:
                    
                    controller_shares_lab7.speed.append(int(self.speed))
                    
                    ## records the current encoder position
                    self.position = self.enc.get_position()*360/4000
                    
                    controller_shares_lab7.position.append(int(self.position))
                    
                    self.next_sample = utime.ticks_add(self.curr_time,self.sample_rate)
                
                ## records the reference velocity at the current time
                self.RPM = self.profile((self.curr_time-self.start_sample)/1000000)
                
                if self.RPM == 0:
                    
                    self.set = 0
                    
                if self.RPM != 0:
                    
                    self.set = self.closedloop.run(self.speed,self.RPM)
                
                self.moe.set_duty(self.set)
                
                if controller_shares_lab7.start == False:
                    
                    self.moe.disable()
                    
                    self.transitionTo(S1_wait)
                
            self.next_time = utime.ticks_add(self.curr_time,self.interval)
                
            
            
    def profile(self,time):
        '''
        A method that return the reference velocity at a specifid time
        @param time the specified time that a reference velocity is desired
        '''
        if time <= 4:
            velocity = 30
        if time <= 7 and time > 4:
            velocity = 0
        if time <= 8 and time > 7:
            velocity = -60
        if time <= 11 and time > 8:
            velocity = 0
        if time <=12 and time > 11:
            velocity = 30*(time-11)
        if time <=13 and time > 12:
            velocity = 30*(1-(time-12))
        if time > 13:
            velocity = 0
            
        return velocity  
     
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        