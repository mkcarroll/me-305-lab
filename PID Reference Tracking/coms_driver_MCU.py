'''
@file coms_driver_MCU.py
@brief a driver for serial communication with an MCU from the MCU side
@author Mitchell Carroll
'''

from pyb import UART

class coms_driver:
    '''
    a communications driver

    '''
    def __init__(self):
        '''
        creates a coms driver object
    
        '''
        
        ## sets up UART comunication with the PC
        self.ser = UART(2)
    def check(self):
        '''
        a method that hecks if data is available
        '''
        if self.ser.any() != 0:
            return True
        else:
            return False
    def send(self,msg):
        '''
        a method that sends data


        '''
        self.ser.write(msg)
    def read(self):
        '''
        a method that reads data
        '''
        val = self.ser.readline()
        return val