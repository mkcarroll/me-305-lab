'''
@file encoder_getter.py
@brief a file which collects position data from a hall effect encoder.
@details this file collects position data when the command is received from
the Data_Hub task and stops when the command is received from the Data_Hub task.
the data is then sent to the Data_Hub task

The following is a finite state diagram for the program:
    
@image html SensorDriverFSM.png "Encoder Getter FSM" width=600px

@author Mitchell Carroll, adapted from code by Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
import utime
import array
import Lab4_Shares

## the initial state of the system
S0_init         = 0

## state 1: wait for command
S1_wait_for_cmd = 1

## state 2: collect data
S2_collect_data = 2

class encoder_getter_task:
    '''
    Encoder task.
    
    This task updates the position of the encoder at a regular interval.
    
    '''
    def __init__(self,encoder,interval,sample_period,sample_time,task_num,dbg=True):
        '''
        creates an encoder task object
        @param encoder passes the encoder object on to the encoder task
        @param interval The time between runs 
        @param sample_period defines how often samples will be taken
        @param sample_time defines how long samples will be taken for
        @param task_num defines what number this task is
        @param dbg a boolean that is true if we want to print debug statements
        '''
        
        ## creates an encoder variable whcih passes in encoder position information
        self.encoder = encoder
    
        
        ## how often in seconds the task will run
        self.interval = int(interval*1000)
        
        ## the sample period in seonds
        self.sample_period = int(sample_period*1000)
        
        ## the time that the encoder will sample for
        self.sample_time = sample_time*1000
        
        ##defines the number of the task
        self.task_num = task_num
        
        ## a boolean that is true if we want to print debug statements
        self.dbg = dbg
        
        ## the time at which the task starts
        self.start_time = utime.ticks_ms()
        
        ## the time that the next iteration will be run
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## defines the initial state of the system
        self.state = S0_init
        
        ## sets the first index value for a iteration variable
        self.idx = 0
        
        ## defines the initial run count
        self.runs = 0
        
    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## records the time that the task runs
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time,self.next_time) >= 0:
            if self.state == S0_init:
                self.printTrace()   
                self.transitionTo(S1_wait_for_cmd)
                
            if self.state == S1_wait_for_cmd:
                self.printTrace()
                if Lab4_Shares.datacol == True:
                    if self.idx == 0:
                        
                        ## defines when the next sample will be taken
                        self.next_sample = utime.ticks_add(utime.ticks_ms(),self.sample_period)
                        
                        ## record when the first sample is taken
                        self.sample_start = utime.ticks_ms()
                        
                        ## a factor that determines the size of the data array
                        self.arrayfact = int(self.sample_time/self.sample_period)
                        
                        ## defines an array that will hold collected data
                        self.encoder_data = array.array('i',self.arrayfact*[0,0])
                        self.encoder_data[0] = 0
                        self.encoder.set_position(0)
                        self.encoder.update()
                        self.encoder_data[1] = self.encoder.pos
                        self.idx += 1
                    if utime.ticks_diff(utime.ticks_ms(),self.next_sample)>=0:
                        self.next_sample = utime.ticks_add(utime.ticks_ms(),self.sample_period)
                        self.encoder.update()
                        self.encoder_data[self.idx*2] = utime.ticks_ms() - self.sample_start
                        self.encoder_data[self.idx*2+1] = self.encoder.pos
                        self.idx += 1
                if Lab4_Shares.datacol == False:                    
                    Lab4_Shares.data = self.encoder_data
                    self.idx = 0
                    Lab4_Shares.datacol = None
                self.runs += 1
                self.next_time = utime.ticks_add(utime.ticks_ms(),self.interval)
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.task_num, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
class encoder_driver:
    '''
    @brief a class that interfaces with the harware and returns states.
    '''
    def __init__(self,timer,pin1,pin2):
        '''
        creates an encoder driver object
        '''
        
        ## specifies which timer is being used
        self.timer = timer
        
        ## specifies which pin corresponds to pin 1
        self.pin1 = pin1
        
        ## specifies which pin corresponds to pin 1
        self.pin2 = pin2
        
        ## records the previous timer count update value
        self.prev_count = 0
        
        ## records the timer count update value
        self.count = 0
        
        ## records teh difference between the last two count updates
        self.delta = 0
        
        ## records the previous corrected position
        self.prev_pos = 0
        
        ## records the corrected position
        self.pos = 0
        
        ##defines pin A6 to cpu pin A6
        self.pinA = pyb.Pin(pin1)
        
        ## defines pin A7 to cpu pin A7
        self.pinB = pyb.Pin(pin2)
        
        ## defines a user specified timer 
        self.tim = pyb.Timer(timer)
        
        ## defines the attributes of the timer
        self.tim.init(prescaler = 0, period = 0xFFFF)
        
        ## defines channel 1 as an encoder channel
        self.ch1 = self.tim.channel(1, mode = pyb.Timer.ENC_AB, pin = self.pinA) # sets up ch 1 as an encoder
        
        ## defines channel 2 as an encoder channel
        self.ch2 = self.tim.channel(2, mode = pyb.Timer.ENC_AB, pin = self.pinB) # sets up ch 2 as an encoder
        
    def update(self):
        '''
        updates the position of the encoder
        '''
        self.prev_count = self.count
        self.count = self.tim.counter()
        self.delta = self.count - self.prev_count
        if self.delta >= self.tim.period()/2:
            self.delta = self.delta - self.tim.period()
        elif self.delta <= -self.tim.period()/2:
            self.delta = self.delta + self.tim.period()
        self.prev_pos = self.pos
        self.pos = self.prev_pos + self.delta
               
    def get_position(self):
        '''
        prints the position of the encoder
        '''
        print('    position: ' + str(self.pos))
        
    def get_delta(self):
        '''
        prints the difference between the last two position updates
        '''
        print('    delta:    ' + str(self.delta))
           
    def set_position(self,position):
        '''
        sets the position of the encoder to a specified value
        @param position the position that the encoder will be set to
        '''
        
        self.pos = position
        self.prev_pos = position
    
        

