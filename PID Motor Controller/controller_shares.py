'''
@file controller_shares.py
@brief a file that contans shared variables for th MCU motor controller
@author Mitchell Carroll
'''

## a boolean that is true to signal the motor to start
start = False

## a variable containing collected data from the encoder
data = [0]