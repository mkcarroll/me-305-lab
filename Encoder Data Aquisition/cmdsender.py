'''
@file cmdsender.py
@brief Sends commands to an MCU that is configured to read an encoder.
@details When called, this script runs a task that sends commands to a nucleo and waits 
for a response. This task is used specifically to collect data from a hall effect
encoder. A 'G' will start data collection, and an 'S' will top data collection. 
data is then transmitted to the cmputer in a batch from the nucleo. The data is also 
processed in this script. Data is recieved as a string and this script parses the string 
and converys to integer. The data is then plotted and exported as a CSV.

The following is a finite state diagram for the system:
    
@image html Lab4UIFSM.png "command sender FSM" width=600px

@author Mitchell Carroll
'''

import serial
import keyboard
import time
import array
import UI_shares
import matplotlib.pyplot as plt
import csv

## Defines the initial state of the system
S0_init              = 0

## the first state of the system is waiting for input
S1_wait_for_input    = 1

## the second state is waiting for a response
S2_wait_for_response = 2

class cmd_sender_task:
    '''
    comand sender task
    
    this task sends data to the nucleo and then waits for a response
    '''    
    def __init__(self,interval,sample_time,sample_period):
        '''
        Creats a CMD_sender_task function
        
        @param interval passes in the frequency that the tak runs
        @param sample_time passes in the interval over which samples are taken
        @param sample_period passes in how often samples are taken
        '''
        
        ## defines how often tasks run
        self.interval = interval
        
        ## sets up UART comunication over COM7
        self.ser = serial.Serial(port='COM3',baudrate=115273,timeout=interval)
        
        ## defines how long samples are recorded
        self.sample_time = sample_time
        
        ## defines how often samples are taken
        self.sample_period = sample_period
        
        ## sets the initial state to S0
        self.state = S0_init
        
        ## records the time at which the task starts
        self.start_time = time.time()
        
        ## defines the time when the task will run next
        self.next_time = self.start_time+self.interval
        
        ## records the initial run number
        self.runs = 0
        
        ## defines the time to stop taking samples
        self.sample_end = None
        
        ## a Boolean that reads true is samples have started being taken
        self.start_idx = False
        
        ## defines the tiem that the task waits before reading the buffer for data
        self.response_time = interval/5  
        
        ## a boolean that reads False if the task should stop running
        self.go = True
                
    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## defines the time when the run starts
        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            if self.state == S0_init:
                
                print('========================')
                print('Input a command')
                print('Available commands:')
                print('Start data collection: G')
                print('Stop data collection:  S')
                print('========================')
                
                self.transitionTo(S1_wait_for_input)
                
            if self.state == S1_wait_for_input:
                if(keyboard.is_pressed('G')):
                    
                    print('you pressed: G')
                    print('Data collection has begun')
                    
                    self.ser.write(str('G').encode('ascii'))
                    
                    ## Records the time when data aquisition begins
                    self.sample_start = time.time()
                    
                    self.sample_end = self.sample_start + self.sample_time
                    self.start_idx = True
                    self.response_next = time.time() + self.response_time
                elif self.start_idx == True and (keyboard.is_pressed('S') or time.time() >= self.sample_end):
                    print('you pressed: S')
                    print('Data collection has stopped')
                    self.ser.write(str('S').encode('ascii'))
                    self.start_idx = False
                    self.transitionTo(S2_wait_for_response)
                    
                    ## specifies the time when the response can be read
                    self.response_next = time.time() + self.response_time
                                                   
            if self.state == S2_wait_for_response:
                if time.time() >= self.response_next:
                    
                    ## contains the read message from UART
                    self.val = self.ser.readline().decode('ascii')
                    if self.val:
                        if self.val[0] == 'a':
                            
                            ## if the message is an array, it is recorded here
                            self.datastr = self.val
                            self.datastr = self.datastr.strip("array('i', [")
                            self.datastr = self.datastr.strip("])")                    
                            data = self.datastr.split(',')
                            
                            for n in range(len(data)):
                                data[n] = data[n].strip(' ')
                                data[n] = int(data[n])
                            self.ser.close()
                            
                            ## contains the timestamps for the data
                            self.time = list(int(len(data)/2)*[0])
                            
                            ## contains position information for the encoder
                            self.pos = list(int(len(data)/2)*[0])
                            for n in range(int(len(data)/2)):
                                self.time[n] = data[2*n]/1000
                                self.pos[n] = data[2*n+1]*360/(28*50)
                            L = 1
                            if time.time()<self.sample_end:
                                while self.time[L] !=0:
                                    L+=1
                                for k in range(L,len(self.time)):
                                    self.time.pop(L)
                                    self.pos.pop(L)
                            print(self.time)
                            print(self.pos)
                            
                            
                            n=0
                            timestr = (1+len(self.time))*[0]
                            timestr[0] = 'Time (s)'
                            posstr = (1+len(self.pos))*[0]
                            posstr[0] = 'Position (Degrees)'
                            for n in range(1,len(self.time)+1):
                                timestr[n] = str(self.time[n-1])
                                posstr[n] = str(self.pos[n-1])
                            rows = [timestr,posstr]
                           # data rows of csv file   
  
                            with open('Data.csv', 'w') as f: 
                                  
                                # using csv.writer method from CSV package 
                                write = csv.writer(f) 
                             
                                write.writerows(rows)
                                
                            plt.figure(1)  
                            plt.plot(self.time,self.pos)
                            plt.xlabel('Time (s)')
                            plt.ylabel('Position (Degrees)')
                            plt.title('Position vs Time')
                            UI_shares.go = False
                        
            self.next_time = self.curr_time + self.interval
            self.runs += 1
    def transitionTo(self,newstate):
            '''
            @brief Updates the variabe that transitions to the next state.
            '''
            self.state = newstate
