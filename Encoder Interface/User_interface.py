'''
@file User_interface.py
@brief User interface task as a finite state machine
@details This script serves as a user interface for the interaction with 
a motor encoder. This script prints available commands and then passes read commands
to another program to handle interpretation.

An FSM diagram for this preogram is shown below:

@image html user_interface_FSM.png "User Interface FSM" width=600px

@author Mitchell Carroll adapted from code by Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
import utime
from pyb import UART

class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    characters. Each character received is checked and if it is an english
    letter a-z it is sent through to cipher to be interpreted.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum an integer that indicates which task is operating
        @param interval An integer number of seconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1000)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        print('====================')
        print('Available Commands:')
        print('Zero:         z')
        print('Get Position: p')
        print('Get Delta:    d')
        print('====================')
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
           
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    shares.cmd = self.ser.readchar()
                    self.ser.writechar(shares.cmd)
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printTrace()
                #Run State 2 Code
                if shares.resp:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    shares.resp = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)