'''
@file encoder_driver.py
@brief
@details
@author Mitchell Carroll
'''
import pyb

class encoder_driver:
    '''
    @brief a class that interfaces with the harware and returns states.
    '''
    def __init__(self,timer,pin1,pin2):
        '''
        creates an encoder driver object
        '''
        
        ## specifies which timer is being used
        self.timer = timer
        
        ## specifies which pin corresponds to pin 1
        self.pin1 = pin1
        
        ## specifies which pin corresponds to pin 1
        self.pin2 = pin2
        
        ## records the previous timer count update value
        self.prev_count = 0
        
        ## records the timer count update value
        self.count = 0
        
        ## records teh difference between the last two count updates
        self.delta = 0
        
        ## records the previous corrected position
        self.prev_pos = 0
        
        ## records the corrected position
        self.pos = 0
        
        ##defines pin A6 to cpu pin A6
        self.pinA = pyb.Pin(pin1)
        
        ## defines pin A7 to cpu pin A7
        self.pinB = pyb.Pin(pin2)
        
        ## defines a user specified timer 
        self.tim = pyb.Timer(timer)
        
        ## defines the attributes of the timer
        self.tim.init(prescaler = 0, period = 0xFFFF)
        
        ## defines channel 1 as an encoder channel
        self.ch1 = self.tim.channel(1, mode = pyb.Timer.ENC_AB, pin = self.pinA) # sets up ch 1 as an encoder
        
        ## defines channel 2 as an encoder channel
        self.ch2 = self.tim.channel(2, mode = pyb.Timer.ENC_AB, pin = self.pinB) # sets up ch 2 as an encoder
        
    def update(self):
        '''
        updates the position of the encoder
        '''
        self.prev_count = self.count
        self.count = self.tim.counter()
        self.delta = self.count - self.prev_count
        if self.delta >= self.tim.period()/2:
            self.delta = self.delta - self.tim.period()
        elif self.delta <= -self.tim.period()/2:
            self.delta = self.delta + self.tim.period()
        self.prev_pos = self.pos
        self.pos = self.prev_pos + self.delta
               
    def get_position(self):
        '''
        prints the position of the encoder
        '''
        return self.pos
        
    def get_delta(self):
        '''
        prints the difference between the last two position updates
        '''
        return self.delta
           
    def set_position(self,position):
        '''
        sets the position of the encoder to a specified value
        @param position the position that the encoder will be set to
        '''
        
        self.pos = position
        self.prev_pos = position
    
        


               