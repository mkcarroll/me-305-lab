'''
@file controller_frontend_lab7.py
@brief a script that sets up data communication from the pc
@details this file sets up a finite state machine that waits for input from the user
in the form of a gain value for the proportional controller and then sends that value
to a microcontroller. this file also received data from the microcontroller,
processes it, and generates a plot of the data.

An FSM diagram for this script can be found at the following link:
    
@image html frontendFSM.png "UI Frontend FSM" width=600px

@author Mitchell Carroll
'''

## the initial state of the system
S0_init = 0

## in this state the task waits for input. this state is blocking
S1_wait_input = 1

## in this state the task waits for a response from the MCU
S2_wait_for_resp = 2

import time
import controller_frontend_shares
import matplotlib.pyplot as plt
from numpy import linspace
import csv

class task_frontend:
    '''
    a controller frontend task
    '''
    def __init__(self,interval,com):
        '''
        creates a task_frontend object
        @param interval specifies the frequency the task will run
        @param com passes in the communication driver

        '''
        
        ## specifiec the frequency that the task will run
        self.interval = interval
        
        ## creates a com driver object
        self.com = com
        
        ## sets the state of the system
        self.state = S0_init
        
        ## records the time that the task first runs
        self.start_time = time.time()
        
        ## records the time that the task will next run
        self.next_time = self.start_time + self.interval
        
        ## records the run count
        self.runs = 0
        
        
    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## records the time that each iteration runs
        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            if self.state == S0_init:
                self.transitionTo(S1_wait_input)
            
            if self.state == S2_wait_for_resp:
                boo = self.com.check()
                if boo:
                    val = self.com.read()
                    self.com.close()
                    
                    ## records the data from the MCU
                    self.datastr = str(val)
                    self.datastr = self.datastr.strip('b"array(\'f\', [')
                    self.datastr = self.datastr.strip('])"')                    
                    data = self.datastr.split(';')
                    for n in range(2):
                        data[n] = data[n].strip('[')
                        data[n] = data[n].strip('"array(\'f\', [')
                        data[n] = data[n].strip('])')
                    speed = data[0].split(',')
                    position = data[1].split(',')
                    t = linspace(0,15,len(speed))
                    ref = []
                    for n in range(len(speed)):
                        speed[n] = speed[n].strip(' ')
                        speed[n] = float(speed[n])
                        
                        position[n] = position[n].strip(' ')
                        position[n] = float(position[n])
                        
                        ref.append(self.profile(t[n]))
                    
                    ## the time against which reference position and speed will be plotted
                    self.tr = []
                    
                    ## reference position
                    self.posr = []
                    
                    with open('reference.csv') as f:
                        reader = csv.reader(f)
                        data = list(reader)
                        
                    for n in range(len(data)):
                        self.tr.append(float(data[n][0]))
                        self.posr.append(float(data[n][2]))
    
                        
                    plt.figure(1)
                    act,= plt.plot(t,speed,'k',label = 'Actual Motor Speed')
                    ref,= plt.plot(t,ref,'r', linestyle = '--', label = 'Reference Speed')
                    plt.ylabel('Speed (RPM)')
                    plt.xlabel('Time (s)')
                    plt.title('Motor Speed Response')
                    plt.legend(handles = [act,ref])
                    
                    plt.figure(2)
                    actp,= plt.plot(t,position,'k',label = 'Actual Shaft Position')
                    refp,= plt.plot(self.tr,self.posr,'r',linestyle = '--',label = 'Theoretical Position')
                    plt.ylabel('Position (Degrees)')
                    plt.xlabel('Time (s)')
                    plt.title('Motor Position Response')
                    plt.legend(handles = [actp,refp])                  
                    
                    Jv = [0]
                    Jp = [0]
                    J = [0]
                    
                    for n in range(1,len(speed)):
                        Jv.append(((float(self.profile(t[n])*360/60) - float(speed[n])*360/60)**2)/len(speed) + Jv[n-1]) 
                        Jp.append(((float(self.profile_pos(t[n])) - float(position[n]))**2)/len(speed) + Jp[n-1])
                        J.append(Jv[n] + Jp[n])
                    print('Velocity Portion J: ' + str(Jv[len(Jv)-1]))
                    print('Position Portion J: ' + str(Jp[len(Jv)-1]))
                    print('Total J: ' + str(J[len(J)-1]))
                    
                    plt.figure(3)
                    vel,= plt.plot(t,Jv,linestyle = '--',label = 'Velocity Error')
                    po,= plt.plot(t,Jp,'r',linestyle = ':',label = 'Position Error')
                    tot,= plt.plot(t,J,'k',label = 'Total Error')
                    plt.ylabel('Error')
                    plt.xlabel('Time (s)')
                    plt.title('Error vs Time')
                    plt.legend(handles = [vel,po,tot]) 
                                        
                    controller_frontend_shares.go = False
                    
            if self.state == S1_wait_input:
                
                ## records the user input Kp value
                self.Kp = str(input('Enter Gain Values Kp,Ki,Kd: '))
                self.com.send(self.Kp.encode())
                self.transitionTo(S2_wait_for_resp)
                
                
            
            self.next_time = self.curr_time + self.interval
            self.runs+=1
            
    def profile(self,time):
        '''
        A method that returns the reference velocity for a given time
        @param time the time at which a reference velocity is desired
        '''
        
        if time <= 4:
            velocity = 30
        if time <= 7 and time > 4:
            velocity = 0
        if time <= 8 and time > 7:
            velocity = -60
        if time <= 11 and time > 8:
            velocity = 0
        if time <=12 and time > 11:
            velocity = 30*(time-11)
        if time <=13 and time > 12:
            velocity = 30*(1-(time-12))
        if time > 13:
            velocity = 0
            
        return velocity  

    def profile_pos(self,time):
        '''
        A method that returns the reference postion for a specified time
        @param time the time at which a reference position is desired
        '''
            
        for n in range(len(self.tr)):
            if time > self.tr[n] and time <= self.tr[n+1]:
                return self.posr[n]
        
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate