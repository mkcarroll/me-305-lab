'''
@file UI_main_lab4.py
@brief sends commands to an MCU that is configured to read an encoder.
@details This script runs one task that sends commands to a nucleo and waits 
for a response. This task is used specifically to collect data from a hall effect
encoder. A 'G' will start data collection, and an 'S' will stop data collection. 
Data is then transmitted to the cmputer in a batch from the nucleo.
@author Mitchell Carroll
'''

import UI_shares
from cmdsender import cmd_sender_task

## interval object, specifies how oftern tasks will run
interval = 0.1

## sample_time object, specifies how long samples will be collected for
sample_time = 10

## sample_period object, specifies the period that samples will be collected
sample_period = 0.2

## Task object for the command sender
task1 = cmd_sender_task(interval,sample_time,sample_period) # Will also run constructor

# To run the task call task1.run() over and over
while UI_shares.go == True: 
    task1.run()