'''
@file closedloop.py
@brief a closed loop controller
@details this script lays out the basic framework of a PID controller. the user inputs
the values for actual speed and reference speed, and the controller outputs the 
PWM level that will be sent to the motor. The user can vary te gain constants using
built in methods (see below). the user can call a method that unwinds the integral term
for the controller as well. To prevent integral windup, the integral term is sign sensitive, so when the controller 
overshoots the cound integral count object (proportianal to time) begins to decrease
until the output is within reasonable range. 
@author Mitchell Carroll
'''

class closedloop:
    '''
    a closed loop controller class
    '''
    def __init__(self,thresh):
        '''
        creates a closedloop object
        @param thresh The acceptable bilateral tolerance for the reference speed
        '''
        
        ## creates a threshhold object
        self.thresh = thresh
        
        ## the proportional gain for the controller
        self.Kp = 0
        
        ## the integral gain for the controller
        self.Ki = 0
        
        ## the derrivative gain for the controller
        self.Kd = 0
        
        ## the proportional comonent of the output level for the controller
        self.prop = 0
        
        ## the integral comonent of the output level for the controller
        self.integ = 0
        
        ## the derrivative comonent of the output level for the controller
        self.deriv = 0
        
        ## the increasing count for the integral term, proportional to time error is occuring
        self.count = 0
        
        ## records the previous error value
        self.errorlast = 0
        
        ## records the previous reference speed
        self.wreflast = 0
        
    def run(self,wact,wref):
        '''
        runs one iteration of the closed loop controller
        @param wact the actual speed of the motor as read by the encoder
        @param wref The reference speed for the motor
        '''
        
        ## an actual speed object
        self.wact = wact
        
        ## creates a reference speed object
        self.wref = wref
        
        error = self.wref - self.wact
        
        # if abs(error) > 3*self.errorlast:
        #     error = self.errorlast
            
        self.prop = self.Kp*error
        
        if self.wref*self.wreflast < 0:
            self.count = 0
        
        if (error - self.thresh) > 0:
            
            self.count +=1
            
            self.integ = self.Ki*self.count
            
        if (error + self.thresh) < 0:
            
            self.count -= 1
            
            self.integ = self.Ki*self.count
        self.deriv = self.Kd*(error - self.errorlast)
        
        self.errorlast = error
        
        self.wreflast = self.wref
            
        level = self.prop + self.integ + self.deriv
        
        return level
        
    def set_Kp(self,Kp):
        '''
        a method that sets the proportional gain for the PID
        @param Kp the proportional gain constant
        '''
        self.Kp = Kp
        
    def set_Ki(self,Ki):
        '''
        a method that sets the integral gain for the PID
        @param Ki the interal gain constant
        '''        
        self.Ki = Ki
        
    def set_Kd(self,Kd):
        '''
        a method that sets the derrivative gain for the PID
        @param Kd the derrivative gain constant
        '''
        self.Kd = Kd
        
    def unwind(self):
        '''
        a method that resets the integral term count and unwinds the integral term
        '''
        self.count = 0
        
    def get_kp(self):
        '''
        a method that returns the proportional gain constant
        '''
        return self.Kp