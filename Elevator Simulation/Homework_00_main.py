'''
@file Homework_00_main.py

@brief This file sets up the finite state machine that simulates the operation
of 2 elevators. This file sets up the multitasking function that allows both
elevators to be running at the same time.


'''

from Homework_00 import Button, MOTOR_DRIVER, taskelevator
        
## Motor Object
MOTOR = MOTOR_DRIVER()

## Button Object for "button 1"
BUTTON_1 = Button('PB6')

## Button Object for "button 2"
BUTTON_2 = Button('PB7')

## Button Object for "upper limit"
UPPER_LIMIT = Button('PB8')

## Button Object for "lower limit"
LOWER_LIMIT = Button('PB9')

## how often the tasks run
interval = 1

## Task 1 object
task1 = taskelevator(MOTOR, BUTTON_1, BUTTON_2, UPPER_LIMIT,LOWER_LIMIT, interval,1) # Will also run constructor

## task 2 object
task2 = taskelevator(MOTOR, BUTTON_1, BUTTON_2, UPPER_LIMIT,LOWER_LIMIT, interval,2)

# To run the task call task1.run() over and over
for N in range(100000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()

