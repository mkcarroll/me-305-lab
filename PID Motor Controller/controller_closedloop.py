'''
@file controller_closedloop.py
@brief A closed loop motor controller
@details this program receives a start and Kp value command from controller_receiver.py.
the controller then used the user defined Kp value to run a proportional motor controller.
the speed values are recorded. The controller stops when signaled by controller_receiver.py.

The following is an FSM diagram from the program:
    
@image html closedloopFSM.png "Closed Loop FSM diagram" width=600px

@author Mitchell Carroll
'''

import utime
import controller_shares

## the intial state of the system
S0_init = 0

## in this state the controller waits for input
S1_wait = 1

## in this state the controller is running the motor
S2_run = 2

class controller_task:
    '''
    a class that contains the contoller task
    '''
    def __init__(self,interval,moe,enc,speed,closedloop):
        '''
        creates a controller_task object
        @param interval specifies the time between runs in microseconds
        @param moe a motor driver object
        @param enc an encoder driver object
        @param speed specifies the setpoint speed for the motor
        @param closedloop passes in the closed loop controller class
        '''
        
        ## defines the time between runs in microseconds
        self.interval = interval
        
        ## a motor driver object
        self.moe = moe
        
        ## an encoder object
        self.enc = enc
        
        ## a closed loop controller object
        self.closedloop = closedloop
        
        ## specifies the motor speed set point in RPM
        self.RPM = float(speed)
        
        ## specifies what level the motor will start at
        self.set = 0
        
        ## defines the initial state
        self.state = S0_init
        
        ## defines the number of runs that the controller has executed
        self.runs = 0
        
        ## records the starting time of the task
        self.start_time = utime.ticks_us()
        
        ## defines the next time when the task will run
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## specifies the period that samples will be taken
        self.sample_rate = 5000

    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## records the time that each iteration runs
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time,self.next_time) >=0:        

            if self.state == S0_init:
                
                self.transitionTo(S1_wait)
                
                
            if self.state == S1_wait:
                
                self.enc.update()
                
                self.enc.set_position(0)
                
                self.closedloop.unwind()
                
                if controller_shares.start:
                    
                    self.moe.enable()
                    
                    self.transitionTo(S2_run)  
                    
                    ## the time that samples begin being recorded
                    self.start_sample = utime.ticks_us()
                    
                    ## the time that the next sample will be recorded
                    self.next_sample = utime.ticks_add(self.start_sample,self.sample_rate)
            
            if self.state == S2_run:
                
                self.enc.update()
                
                ## Records the current speed of motor
                self.speed = self.enc.get_delta()*60/(4000*self.interval/(1000000))
                
                if utime.ticks_diff(self.curr_time,self.next_sample)>=0:
                    
                    controller_shares.data.append(int(self.speed))
                    
                    self.next_sample = utime.ticks_add(self.curr_time,self.sample_rate)
                
                ## records the differene between the set point ans actual RPM
                self.set = self.closedloop.run(self.speed,self.RPM)
                self.moe.set_duty(self.set)
                
                if controller_shares.start == False:
                    
                    self.moe.disable()
                    
                    self.transitionTo(S1_wait)
                
            self.next_time = utime.ticks_add(self.curr_time,self.interval)
                
            
            
           
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        