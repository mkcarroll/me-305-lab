'''
@file controller_receiver_lab7.py
@brief A script that sets up data communication and timing
@details this file receives three gain values from the PC front end. when these 
values are received, the program signals the closed loop controller to begin
using the user defined Kp, Ki, and Kd values. This program signals the controller to stop
after 2 seconds and the transmits the data colleted from the encoder to the user
computer.

The following is an FSM diagram from the program:
    
@image html receiverFSM.png "Receiver FSM diagram" width=600px

@author Mitchell Carroll
'''

from pyb import UART
import utime
import controller_shares_lab7
import array

## the initial state of the system
S0_init = 0

## in this state the controller waits for input
S1_wait = 1

## in this state the controller is collecting data
S2_collect = 2

## in this state the controller is sending data
S3_send = 3

class receiver_task:
    '''
    Receiver task
    
    this task interprets commands from the UI and sends data to the UI
    '''
    def __init__(self,interval,com,closedloop):
        '''
        creates a Data_Hub task object
        @param interval defines how often the task will run
        @param com passes in the construced coms driver
        @param closedloop passes in a closed loop controller class
        '''
        ## defines the initial state of the system
        self.state = S0_init
        
        ## defines how much time in seonds between task runs
        self.interval = interval
        
        ## passes in the constructed coms driver
        self.com = com
        
        ## a closed loop controller object
        self.closedloop = closedloop

        ## creates serial communication variable
        self.ser = UART(2)
        
        ## sets the start time
        self.start_time = utime.ticks_us()
        
        ## defines the next time that the task will run
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## a boolean that returns True if the data collection has started
        self.start_ind = False
        
        ## sets the initial run count
        self.runs = 0
        
        ## specifies how long samples will be taken for
        self.sample_time = 15000000
        
        ## a variable that reads 1 if data is available and 0 if data is ready to be read
        self.idx = 1
        
        

        
    def run(self):
        '''
        runs one iteration of the task
        '''
        ## defines the time that the task starts running
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time,self.next_time) >= 0:
            
            if self.state == S0_init:
                
                self.transitionTo(S1_wait)
                
                
            if self.state == S1_wait:
                
                boo = self.com.check()
                
                if boo and self.idx == 1:
                    
                    ## records the time that data first enters the buffer
                    self.msg_start = utime.ticks_us()
                    
                    ## records the time that the data will be ready to be read
                    self.msg_read = utime.ticks_add(self.msg_start,10000)
                    
                    self.idx=0
                    
                if (self.idx == 0) and (utime.ticks_diff(self.curr_time,self.msg_read)>=0):

                    ## a variable that contains the data read from the buffer
                    self.val = str(self.com.read())
                    
                    self.val = self.val.strip('b')
                    
                    self.val = self.val.strip("'")
                    
                    ## A variable that contains the user input gains
                    self.gains = self.val.split(',')

                    Kp =float(self.gains[0])
                    
                    Ki =float(self.gains[1])
                    
                    Kd =float(self.gains[2])
                    
                    self.closedloop.set_Kp(Kp)
                    
                    self.closedloop.set_Ki(Ki)
                    
                    self.closedloop.set_Kd(Kd)
                    
                    controller_shares_lab7.start = True
                    
                    controller_shares_lab7.speed = array.array('f',[0])
                    
                    controller_shares_lab7.position = array.array('f',[0])
                    
                    self.transitionTo(S2_collect)
                                        
                    ## records the time that samples begin being taken
                    self.sample_start = utime.ticks_us()
                    
                    ## records the time when samples will end
                    self.sample_end = utime.ticks_add(self.sample_start,self.sample_time)
                    
            
            if self.state == S3_send:
                
                controller_shares_lab7.speed = str(controller_shares_lab7.speed)
                
                controller_shares_lab7.position = str(controller_shares_lab7.position)
                
                self.com.send(controller_shares_lab7.speed + ';')
                
                self.com.send(controller_shares_lab7.position)
                
                self.idx = 1
                
                self.transitionTo(S1_wait)
            
            if self.state == S2_collect:
                
                ## records the current time on each iteration
                self.sample_curr = utime.ticks_us()
                
                if utime.ticks_diff(self.sample_curr,self.sample_end)>=0:
                    
                    controller_shares_lab7.start = False
                    
                    self.transitionTo(S3_send)
            
                    

            
            self.next_time = utime.ticks_add(self.curr_time,self.interval)
                    
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        
