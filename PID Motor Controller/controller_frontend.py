'''
@file controller_frontend.py
@brief a script that sets up data communication from the pc
@details this file sets up a finite state machine that waits for input from the user
in the form of a gain value for the proportional controller and then sends that value
to a microcontroller. this file also received data from the microcontroller,
processes it, and generates a plot of the data.

An FSM diagram for this script can be found at the following link:
    
@image html frontendFSM.png "UI Frontend FSM" width=600px

@author Mitchell Carroll
'''

## the initial state of the system
S0_init = 0

## in this state the task waits for input. this state is blocking
S1_wait_input = 1

## in this state the task waits for a response from the MCU
S2_wait_for_resp = 2

import time
import controller_frontend_shares
import matplotlib.pyplot as plt
from numpy import linspace

class task_frontend:
    '''
    a controller frontend task
    '''
    def __init__(self,interval,com):
        '''
        creates a task_frontend object
        @param interval specifies the frequency the task will run
        @param com passes in the communication driver

        '''
        
        ## specifiec the frequency that the task will run
        self.interval = interval
        
        ## creates a com driver object
        self.com = com
        
        ## sets the state of the system
        self.state = S0_init
        
        ## records the time that the task first runs
        self.start_time = time.time()
        
        ## records the time that the task will next run
        self.next_time = self.start_time + self.interval
        
        ## records the run count
        self.runs = 0
    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## records the time that each iteration runs
        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            if self.state == S0_init:
                self.transitionTo(S1_wait_input)
            
            if self.state == S2_wait_for_resp:
                boo = self.com.check()
                if boo:
                    val = self.com.read()
                    self.com.close()
                    
                    ## records the data from the MCU
                    self.datastr = str(val)
                    print(self.datastr)
                    self.datastr = self.datastr.strip("b'[")
                    self.datastr = self.datastr.strip("]")                    
                    data = self.datastr.split(',')
                    
                    for n in range(len(data)):
                        data[n] = data[n].strip(' ')
                        data[n] = int(data[n])
                    t = linspace(0,2,len(data))
                    ref=(len(data))*[1200]
                    act,= plt.plot(t,data,'k',label = 'Actual Motor Speed')
                    ref,= plt.plot(t,ref,'r',label = 'Reference Speed')
                    plt.ylabel('Speed (RPM)')
                    plt.xlabel('Time (s)')
                    plt.title('Transient Motor Response')
                    plt.legend(handles = [act,ref])                
                    
                    controller_frontend_shares.go = False
                    
            if self.state == S1_wait_input:
                
                ## records the user input Kp value
                self.Kp = str(input('Enter Gain Values Kp,Ki,Kd: '))
                self.com.send(self.Kp.encode())
                self.transitionTo(S2_wait_for_resp)
                
                
            
            self.next_time = self.curr_time + self.interval
            self.runs+=1
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate