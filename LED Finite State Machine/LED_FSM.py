'''
@file LED_FSM.py

@brief This file creates a finite state machine that blinks an LED.

@details This script creates a finite element machine that will blink and LED. 
This script turns the LED on and then after a certain period of time has
past, checks the state of the LED. If the LED is on, it will be turned off,
and if it is off, it will be turned one. 

'''
import time

class task_LED_blink:
    '''
    @brief      A finite state machine that blinks an LED.
    @details    This class implements a finite element machine that blinks
    a virtual LED.
    '''
    
    ## defines the initial state
    S0_INIT    = 0
    
    ## defines the state when the LED is on 
    S1_LED_ON  = 1
    
    ## defines the state when the LED is off
    S2_LED_OFF = 2

    def __init__(self,LED,interval):
        '''
        @brief Creates a task_LED_blink object.
        '''
        
        ## sets the starting state to S0
        self.state = self.S0_INIT
        
        ## creates a variable that passes in information about the LED
        self.LED = LED
        
        ## the number of runs for the task
        self.runs = 0
        
        ## records the time that the task starts
        self.start_time = time.time()
        
        ## how often the tasks run
        self.interval = interval
        
        ## the time that the next task will run
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief Runs one iteration of task_LED_blink.
        '''
        
        ## the time that the task begins running
        self.current_time = time.time()
        if(self.current_time >= self.next_time):
        
            if(self.state == self.S0_INIT):
                
#                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.current_time - self.start_time))
                self.LED.ON()
                self.transitionTo(self.S1_LED_ON)
                
            elif(self.state == self.S1_LED_ON):
#                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.current_time - self.start_time))
                self.LED.OFF()
                self.transitionTo(self.S2_LED_OFF)
                
            elif(self.state == self.S2_LED_OFF):
                
#                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.current_time - self.start_time))
                self.LED.ON()
                self.transitionTo(self.S1_LED_ON)
                
            self.next_time = self.current_time + self.interval
            self.runs +=1
            
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        
class LED_driver:
    '''
    @brief      A class that drives a virtulal LED.
    '''
    
    def __init__(self):
        '''
        @brief      Creates a LED driver object.
        '''
        pass
    def ON(self):
        '''
        @brief     turns the LED on.
        '''
        print('LED: ON')
    def OFF(self):
        '''
        @brief     turns the LED off.
        '''
        print('LED: OFF')