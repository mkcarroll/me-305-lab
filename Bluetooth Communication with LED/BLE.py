'''
@file BLE.py
@brief this file is a driver for the bluetooth module
@details This file contains a class that drives the bluetooth module. it contains
a check method to check if data is available, a read method to read the data, a 
write method to write bluetooth data, and an LED lethod which can turn an LED on 
or off.
@author Mitchell Carroll
'''

import pyb

class BLE_driver:
    '''
    class BLE driver
    
    a driver that can read, write, and check for data via bluetooth
    '''
   
    def __init__(self):
        '''
        creates a BLE_driver object
        '''
        
        ## defines the uart settings for this script
        self.ser = pyb.UART(3,9600)
        
        ## defines pinA5 to cpu pin A5
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    
    def check(self):
        '''
        a method that checks if bluetooth data is available
        '''
        if self.ser.any() != 0:
            return True
        else:
            return False
    def read(self):
        '''
        a method that reads bluetooth data
        '''

        val = self.ser.readline()
        return val
    
    def write(self,msg):
        
        '''
        a method that writes bluetooth data
        '''
        self.ser.write(msg)
    def LED(self,state):
        '''
        a method that turns an LED on or off
        '''
        
        ## defines the state of an LED 1 is on 0 is off
        self.state = state
        if self.state == 0:
            self.pinA5.low()
            # print('LED off')
        if self.state == 1:
            self.pinA5.high()
            # print('LED on')