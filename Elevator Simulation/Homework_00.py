'''
@file Homework_00.py

@brief This file creates a finite state machine that runs elevators

@details This script creates a finite element machine that will run two 
elevators. This script first waits until the buttons are cleared and then
initializes by moving the the first floor and waits for a command. 

The following is an FSM diagram for the system

@image html elevatorFSM.png "Homework 00 FSM Diagram" width=600px

'''
import time
from random import choice

class taskelevator:
    '''
    @brief      A finite state machine that runs an elevator.
    @details    This class implements a finite element machine that simulates
    the opperation of 2 elevators.
    '''
    ## the initial state of the system
    S0_INIT        = 0
    
    ## the state defined when the elevator is moving down
    S1_MOVING_DOWN = 1
    
    ## the state defined when the elevator is moving up
    S2_MOVING_UP   = 2
    
    ## the state defined when the elevator reaches the first floor
    S3_FLOOR_1     = 3
    
    ## the state defined when the elevator reaches the second floor
    S4_FLOOR_2     = 4
    

    def __init__(self,MOTOR,BUTTON_1,BUTTON_2,UPPER_LIMIT,LOWER_LIMIT,interval,number):
        '''
        @brief Creates a taskelevator object.
        '''
        
        ## the first state in the system is init
        self.state = self.S0_INIT
        
        ## creates a motor variable which passes in motor object information
        self.MOTOR = MOTOR
        
        ## creates a button 1 variable which passes in button 1 information
        self.BUTTON_1 = BUTTON_1
        
        ## creates a button 2 variable which passes in button 2 information
        self.BUTTON_2 = BUTTON_2
        
        ## creates an upper limit variable which reads true when the upper limit is reached
        self.UPPER_LIMIT = UPPER_LIMIT
        
        ## creates a lower limit variable which reads true when the lower limit is reached
        self.LOWER_LIMIT = LOWER_LIMIT
        
        ## how many times the task has been run
        self.runs = 0
        
        ## records the time when the script begins
        self.start_time = time.time()
        
        ## the ammount of time between runs
        self.interval = interval
        
        ## the elevator number
        self.number = number
        
        ## the time that the task will next run
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief Runs one iteration of taskelevator.
        '''
        
        ## the time that the task is executed
        self.current_time = time.time()
        if(self.current_time >= self.next_time):
        
            if(self.state == self.S0_INIT):
                
                #print(str(self.runs) + ' State 0 {:0.2f}'.format(self.current_time - self.start_time))
                if(self.BUTTON_1.getButtonState() == False and self.BUTTON_2.getButtonState() == False):
                    self.MOTOR.DOWN(self)
                    self.transitionTo(self.S1_MOVING_DOWN)
                
            elif(self.state == self.S1_MOVING_DOWN):
                #print(str(self.runs) + ' State 1 {:0.2f}'.format(self.current_time - self.start_time))
                if(self.LOWER_LIMIT.getButtonState() == True):
                    self.MOTOR.OFF(self)
                    print('FLoor: 1')
                    self.transitionTo(self.S3_FLOOR_1)
                
            elif(self.state == self.S2_MOVING_UP):
                
                #print(str(self.runs) + ' State 2 {:0.2f}'.format(self.current_time - self.start_time))
                if(self.UPPER_LIMIT.getButtonState() == True):
                    self.MOTOR.OFF(self)
                    print('Floor: 2')
                    self.transitionTo(self.S4_FLOOR_2)
                
            elif(self.state == self.S3_FLOOR_1):
                
               # print(str(self.runs) + ' State 3 {:0.2f}'.format(self.current_time - self.start_time))
                if(self.BUTTON_2.getButtonState() == True):
                    self.MOTOR.UP(self)
                    self.transitionTo(self.S2_MOVING_UP)
                
            elif(self.state == self.S4_FLOOR_2):
                
                #print(str(self.runs) + ' State 4 {:0.2f}'.format(self.current_time - self.start_time))
                if(self.BUTTON_1.getButtonState() == True):
                    self.MOTOR.DOWN(self)
                    self.transitionTo(self.S1_MOVING_DOWN)
                
            self.next_time = self.current_time + self.interval
            self.runs +=1
            
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate
        
class MOTOR_DRIVER:
    
    '''
    @brief      A motor driver class.
    @details    This class represents a motor driver although instead of actually
                driving a motor, it prints what the motor should be doing. yay
    '''
    
    def __init__(self):    
        '''
        @brief      Creates a motor driver object.
        '''
        pass
    
    def OFF(self, taskelevator):
        '''
        @brief      Turns the motor off.
        @param taskelevator  Passes in attributes of class taskelevator.
        '''
        print('elevator: ' + str(taskelevator.number))
        print('motor off')
        
        
    def UP(self, taskelevator):
        '''
        @brief     Turns the motor on in the upward direction.
        @param taskelevator  Passes in attributes of class taskelevator.
        '''
        print('elevator: ' + str(taskelevator.number))
        print('motor moving up')
        
        
    def DOWN(self, taskelevator):
        '''
        @brief     turns the motor on in the downward direction
        @param taskelevator  passes in attributes of class taskelevator
        '''
        
        print('elevator: ' + str(taskelevator.number))
        print('motor moving down')
        
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary elevator occupent to select which floor to go to.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])