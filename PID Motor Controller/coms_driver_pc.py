'''
@file coms_driver_pc.py
@brief a driver for MCU communicaiton on the PC side
@author Mitchell Carroll
'''

import serial

class coms_driver:
    '''
    a communicaitons driver
    '''
    def __init__(self,interval):
        '''
        creates a coms driver object

        '''
        
        ## sets up serial communication with com3
        self.ser = serial.Serial(port = 'COM3', baudrate = 115273,timeout = 1)
    def check(self):
        '''
        a method that hecks if data is available
        '''
        if self.ser.inWaiting() != 0:
            return True
        else:
            return False
    def send(self,msg):
        '''
        a method that sends data


        '''
        self.ser.write(msg)
    def read(self):
        '''
        a method that reads data
        '''
        val = self.ser.readline()
        return val
    def close(self):
        '''
        a method that closes serial coms

        '''
        self.ser.close()
        