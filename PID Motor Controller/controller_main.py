'''
@file controller_main.py
@brief a script that controls task timing for the MCU
@author Mitchell Carroll
'''

from encoder_driver import encoder_driver
from motor_driver import motor_driver
from coms_driver_MCU import coms_driver
from controller_closedloop import controller_task
from controller_receiver import receiver_task
from closedloop import closedloop
import pyb

## defines the setpoint speed for the motor in RPM
speed = 1200

## defines the bilateral tolerance for the seed setpoint in RPM
thresh = 25

## specifies the frequency that each task will run at
interval = 5000

## specifies the pin for motor 1 pin 1
pinM1P1 = pyb.Pin.cpu.B0

## specifies the pin for motor 1 pin 2
pinM1P2 = pyb.Pin.cpu.B1

## specifies the sleep pin for the h bridge
sleep = pyb.Pin.cpu.A15

## specifies the timer used by the encoder
timerenc = 8

## specifies the first pin used by the encoder
pinE1 = pyb.Pin.cpu.C6

## specifies the second pin used by the encoder
pinE2 = pyb.Pin.cpu.C7

## creates a motor driver object
moe = motor_driver(pinM1P1,pinM1P2,sleep)

## creates an encoder object
enc = encoder_driver(timerenc,pinE1,pinE2)

## creates a com driver object
com = coms_driver()

## creates a closedloop controller object
closedloop = closedloop(thresh)

## creates a task1 object
task1 = controller_task(interval,moe,enc,speed,closedloop)

## creates a task2 object
task2 = receiver_task(interval,com,closedloop)

while True:
    task1.run()
    task2.run()