'''
@file motor_driver.py
@brief a scrips that drives a motor
@author Mitchell Carroll
'''

import pyb

class motor_driver:
    '''
    @brief a class that drives a DC motor with PWM
    '''
    
    def __init__(self,pin1,pin2,sleep):
        '''
        creates a motor driver object
        @param pin1 defines motor pin 1
        @param pin2 motor pin 2
        @param sleep defines the MCU pin on the H-bridge

        '''
        
        ## sets pinA15 to cpu pin A15
        self.sleep = sleep
        
        ## sets up the sleep pin as a toggle high low
        self.toggle = pyb.Pin(sleep,pyb.Pin.OUT_PP)
        
        tim3 = pyb.Timer(3,freq = 20000)
        
        ## sets up timer channel 1 as PWM on motor 1 pin 1 
        self.t3ch3 = tim3.channel(3,pyb.Timer.PWM,pin = pin1)
        
        ## sets up timer channel 2 as PWM on motor 1 pin 2
        self.t3ch4 = tim3.channel(4,pyb.Timer.PWM,pin = pin2)
        
        
    def enable(self):
        '''
        A method that trns the motors on
        '''
        self.sleep.high()
        
    def disable(self):
        '''
        A method that turns the motors off
        '''
        self.sleep.low()
        
    def set_duty(self,duty):
        '''
        A method that sets the PWM duty cycle for a given motor
        @param duty specifies the duty cycle that the specified motor will opperate at
        '''
        
        if duty <= 0:
            self.t3ch3.pulse_width_percent(0)
            self.t3ch4.pulse_width_percent(abs(duty))
        if duty > 0:
            self.t3ch3.pulse_width_percent(duty)
            self.t3ch4.pulse_width_percent(0)
        