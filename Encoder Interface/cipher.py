'''
@file cipher.py
@brief Character cipher waiting for intermittent use by the user
@details This script interprets characters input by the user and executes actions 
related to reading a motor encoder. 

The following is an FSM diagram for the program:
    
@image html cipherFSM.png "Cipher FSM" width=600px

@author Mitchell Carroll, adapted from code by Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
import utime
import shares

class cipher:
    '''
    Cipher task.
    
    An object of this class encodes characters sent by a User_interface object. 
    The chosen cipher is to execute from a list of 3 commands for the motor encoder
        
    
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    

    def __init__(self, taskNum, interval, encoder, dbg=True):
        '''
        Creates a cipher task object.
        @param taskNum A number to identify the task
        @param interval An integer number of seconds between desired runs of the task
        @param encoder passes the encoder object in to the cipher task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1000)
        
        ## creates an encoder variable whcih passes in encoder position information
        self.encoder = encoder
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created scaler task')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                # Run State 1 Code
                if shares.cmd:
                   self.cipher(shares.cmd,self.encoder)
                   shares.resp = shares.cmd
                   shares.cmd = None
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
    def cipher(self, cmd, encoder):
        '''
        executes commands for the encoder z = zero p = get position d = get delta
        '''
        if cmd == 122:
            
            encoder.set_position(0)
            encoder.get_position()
            
        elif cmd == 112:
            
            encoder.get_position()
            
        elif cmd == 100:
            
            encoder.get_delta()
