'''
@file encoder_main.py
@brief a file which reads the position of a hall effect encoder.
@details This script runs 3 tasks, the first sets up the encoder task which reads raw data from the encoder and returns position when prompted
the second sets up a user interface through which the user can communicate with
the encoder and query for position and delta as well as zero the sensor. The
third is a cipher that interprets user input and transforms commands into outputs.
@author Mitchell Carroll
'''

import shares
from encoder import encoder_task, encoder_driver
from User_interface import TaskUser
from cipher import cipher
import pyb

## timer object, input any valid timer pin combination
timer = 3

## pin 1 object input, any valid pin timer configuration
pin1 = pyb.Pin.cpu.A6

## pin 2 object input, any valid pin timer configuration
pin2 = pyb.Pin.cpu.A7

## interval object, specifies how oftern tasks will run
interval = 0.1

## encoder Object
encoder = encoder_driver(timer,pin1,pin2)

## Task object for the encoder
task1 = encoder_task(encoder,interval) # Will also run constructor

## task object for the user interface
task2 = TaskUser(2,interval,dbg = False)

## task object for the cipher
task3 = cipher(3,interval,encoder,dbg = False)

# To run the task call task1.run() over and over
while True: 
    task1.run()
    task2.run()
    task3.run()