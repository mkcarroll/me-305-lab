'''
@file Lab4_Shares.py
@brief A container for all the inter-task variables
@author Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The command character sent from the user interface task to the cipher task
datacol = None

## The response from the cipher task after encoding
data    = None