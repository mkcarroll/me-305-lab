'''
@file UI_shares.py
@brief a container for all inter-task variables
@author: Mitchell Carroll
'''

## a boolean that returns False if the task should stop running
go = True