'''
@file LED_main.py

@brief This file sets up a multitasking function for powering LEDs
@details this file sets up a multitasking function that will blink a virtual
LED and ramp a physical LED simultaneously.


'''

from LED_FSM import LED_driver, task_LED_blink
from LED_ramp import task_LED_ramp
        
## LED Object
LED = LED_driver()

## how often the tasks run
interval = 1

## how fast the LED will ramp. 5 percent/ms ramprate
ramprate = 100

## Task 1 object
task1 = task_LED_blink(LED,interval) # Will also run constructor

## Task 2 object
task2 = task_LED_ramp(LED,ramprate)

# To run the task call task1.run() over and over
while True: 
    task1.run()
    task2.run()

