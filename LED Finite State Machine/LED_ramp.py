'''
@file LED_ramp.py

@brief This file creates a task that lights an LED in a ramp pattern.

@details This script creates atask that will ramp an LED from 0 brightness 
to 100 percent over a specified time interval. 

'''

import utime
import pyb

## defines which pin the LED is on
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

## specifies which timer will be used and with what atributes
tim2 = pyb.Timer(2, freq = 20000)

## sets up channel 1 as PWM
t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin = pinA5)
class task_LED_ramp:
    '''
    @brief a class that ramps an LED from 0 to 100 percent brightness.
    '''
    def __init__(self,LED,ramprate):
        '''
        @brief Creates a task_LED_ramp object.
        '''
        
        ## creates an LED variable that passes in LED information
        self.LED = LED
        
        ## creates a ramprate variable that passes in ramprate information
        self.ramprate = ramprate
        
        ## records the time that the task has started
        self.start_time = utime.ticks_ms()
        
        ## defines the time when the task will run next
        self.next_time = self.start_time + self.ramprate
        
        ## defines the brightness of the LED
        self.brightness = 0
        
    def run(self):
        '''
        @brief runs one iteration of task_LED_ramp.
        '''
        
        ## records the time that the run has started
        self.cur_time = utime.ticks_ms()
        if self.cur_time > self.next_time:        
            self.brightness +=5
            t2ch1.pulse_width_percent(self.brightness)
            if self.brightness == 100:
                self.brightness = 0
            self.next_time = self.ramprate + utime.ticks_ms()