'''
@file BLE_FSM.py
@brief this file flashes an LED with frequency from bluetooth input
@details this file sets up a finite state machine that waits for a frequency
input then when received flashes an LED at the user specified frequency. the 
script then sends a response to the user indicating what frequency is set.

The following is a finite state diagram for the system:
    
@image html BLEFSM.png "Bluetooth LED flasher FSM" width=600px

@author Mitchell Carroll
'''
import pyb
import utime

## defines the initial state
S0_init = 0

## defines the waiting for inout state
S1_wait = 1

## defines the send response state
S2_send = 2

class BLE_task:
    '''
    BLE task
    
    A class that falshes an LED at a frequency defined over bluetooth
    '''
    def __init__(self,BT,interval):
        '''
        creates a BLE_task function
        
        @param BT passes in the constructed bluetooth driver
        @param interval defines the period at which the task will run
        '''
        
        ## creates a BT object
        self.BT = BT
        
        ## defines the period at which the task will run
        self.interval = interval
        
        ## records the time that the task is constructed
        self.start_time = utime.ticks_us()
        
        ## defines the time when the task will next run
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## defines th initial state of the system
        self.state = S0_init
        
        ## defines the initial frequency of the LED
        self.frequency = 0
        
        ## records whether or not the LED has started flashing
        self.start = 0
        
        ## records if data has been received
        self.data = 1
        
    def run(self):
        '''
        runs one iteration of the task
        '''
        
        ## records the time that the task runs
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time,self.next_time) >= 0:
            
            if self.state == S0_init:
                self.transitionTo(S1_wait)
                
            if self.state == S1_wait:
                if self.BT.check() and self.data == 1:
                    self.data = 0
                    
                    ## defines the time when data is available
                    self.wait = utime.ticks_us()
                    
                    ## defines the time that data will be read
                    self.read_time = utime.ticks_add(self.wait,1000)
                    
                if self.data == 0:
                    if utime.ticks_diff(utime.ticks_us(),self.read_time)>=0:
                        self.data = 1
                        
                        ## defines a variable that contains the received data
                        self.val = self.BT.read()
        
                        if self.val:
                            self.frequency = float(self.val)
                            if self.frequency == 0:
                                self.BT.LED(0)
                                self.start = 0
                            if self.frequency != 0:
                                
                                ## defines the user selected period
                                self.period = 1000000/self.frequency
                                
                                ## records the time that the LED starts flashing
                                self.flash_start = utime.ticks_us()
                                
                                ## defines the next time that the LED will toggle
                                self.next_toggle = utime.ticks_add(self.flash_start,int(self.period/2))
                                self.BT.LED(1)
                                
                                ## an index that is 1 if the LED is on and 0 if off
                                self.idx = 1
                                self.start = 1
                            self.transitionTo(S2_send)
                
            if self.state == S2_send:
                self.BT.write('Frequency: ' + str(self.frequency) + 'Hz ')
                self.transitionTo(S1_wait)
                
            self.next_time = utime.ticks_add(self.curr_time,self.interval)  
            ## records th current time when the fash conditions are about to be checked
            self.flash_time = utime.ticks_us()
            if self.start == 1:
                if utime.ticks_diff(self.flash_time,self.next_toggle) >= 0:
                    
                    ## an index that is 1 if the LED has not yet toggled
                    self.id = 1
                    if self.idx == 1 and self.id == 1:
                        self.BT.LED(0)
                        self.idx = 0
                        self.id = 0
                    if self.idx == 0 and self.id == 1:                            
                        self.BT.LED(1)
                        self.idx = 1
                    self.next_toggle = utime.ticks_add(self.flash_time,int(self.period/2))
                  
            
        
            
            
    def transitionTo(self,newstate):
        '''
        @brief Updates the variabe that transitions to the next state.
        '''
        self.state = newstate