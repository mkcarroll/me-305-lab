'''
@file Lab7_main.py
@brief a script that sets up task timing for an MCU
@author Mitchell Carroll
'''

from controller_frontend_lab7 import task_frontend
from coms_driver_pc import coms_driver
import controller_frontend_shares


## specifies the frequency that each task will run
interval = 1

## creates a com driver object
com = coms_driver(interval)

## creates a task1 object
task1 = task_frontend(interval,com)

while controller_frontend_shares.go:
    task1.run()